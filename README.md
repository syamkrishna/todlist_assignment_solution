  1. Make a Directory & clone this project on your directory using =====>

       git clone https://gitlab.com/KarthikaPJ_KBA/todlist_assignment_solution.git

  2. npm install

  3. npm install web3@1.0.0-beta.48 --save 

  4. Deploy TodoList.sol file using remix ide and geth private chain

  5. Use Deplyed ContractAddress and ABI in app.js

  6. Run application using "npm start"

  7. Check your http://localhost:3000/
