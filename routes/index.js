var express = require('express');
var router = express.Router();



router.get('/', function (req, res, next) {

  ToDo.methods.taskCount().call().then(function (taskCount) {

    console.log("Count: ", taskCount);
    var taskArr = [];

    for (var i = 1; i <= taskCount; i++) {
      ToDo.methods.tasks(i).call().then(function (task) {
        console.log("task: ", task)

        taskArr.push({ id: task[0], content: task[1], status: task[2] });
        if (taskArr.length == taskCount)
          res.render("index", { taskArray: taskArr })
      })
    }
  })
});

router.post('/createTask', function (req, res, next) {
  reqData = req.body;
  console.log(reqData);
  task = req.body.Task;
  web3.eth.getAccounts().then((data1) => {
    ToDo.methods.createTask(task).send({ from: data1[0] }).then((data) => {
      console.log("CreateTask: ", data);
      res.redirect('/');
    })
  })
});

router.get('/completeTask', function (req, res, next) {
  var taskID = req.query.taskid;
  web3.eth.getAccounts().then((data1) => {
    ToDo.methods.toggleCompleted(taskID).send({ from: data1[0] }).then((data) => {
      console.log("TaskToggle : ", data);
      res.redirect('/');
    })
  })
});


module.exports = router;


